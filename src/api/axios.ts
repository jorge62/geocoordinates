import axios from "axios";

export const axiosUserApi = axios.create({
  baseURL: process.env.REACT_APP_USER_URL,
});

export const axiosProvinceApi = axios.create({
  baseURL: process.env.REACT_APP_PROVINCE_URL,
});
