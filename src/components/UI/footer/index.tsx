import * as React from "react";
import styles from "./styles.module.scss";

export const Footer: React.FC = () => {
  return (
    <React.Fragment>
      <footer className={styles.footer}>
        <div>&copy; React Coordinates - 2023</div>
      </footer>
    </React.Fragment>
  );
};
