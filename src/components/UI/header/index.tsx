import * as React from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import styles from "./styles.module.scss";

export const Header = () => {
  const navigate = useNavigate();
  const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false);

  React.useEffect(() => {
    const userToken = localStorage.getItem("user-token");
    if (!userToken || userToken === "undefined") {
      setIsLoggedIn(false);
    } else {
      if (userToken.length > 0) {
        setIsLoggedIn(true);
      }
    }
  }, []);

  const handleLogout = () => {
    localStorage.clear();
    setTimeout(() => {
      navigate("/auth/login");
    }, 500);
  };

  return (
    <div className={styles.header}>
      <p className={styles.titleHeader}>React Coordinates</p>
      {isLoggedIn && (
        <div>
          <button
            className={styles.logout}
            onClick={() => {
              handleLogout();
            }}
          >
            Logout
          </button>
        </div>
      )}
    </div>
  );
};
