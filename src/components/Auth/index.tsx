import { Footer } from "components/UI/footer";
import { Header } from "components/UI/header";
import { PageLogin } from "pages/Login";
import React from "react";

export const Auth: React.FC = () => {
  return (
    <React.Fragment>
      <Header />
      <PageLogin />
      <Footer />
    </React.Fragment>
  );
};
