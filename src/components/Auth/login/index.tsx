import React from "react";
import { useNavigate } from "react-router-dom";
import { useForm, SubmitHandler } from "react-hook-form";
import { useState } from "react";
import styles from "./styles.module.scss";
import { useEffect } from "react";
import { axiosUserApi } from "api/axios";
import { ReqLoggerUserApi } from "interceptors/interceptors";
import Swal from "sweetalert2";

interface Inputs {
  username: string;
  password: string;
}

export const Login: React.FC = () => {
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<Inputs>();

  const [user, setUser] = useState<{ username: string; password: string }>({
    username: "",
    password: "",
  });
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const checkUserToken = async () => {
    const userToken = localStorage.getItem("user-token");
    if (!userToken || userToken === "undefined") {
      setIsLoggedIn(false);
      return navigate("/auth/login");
    } else {
      try {
        const response = await axiosUserApi.get("/user/");
        /* eslint-disable */
        ReqLoggerUserApi;
        if (response.data) {
          const data = response.data;
          const token = data.token;
          if (token !== userToken) {
            alert("No puede ingressar. Por favor intentelo mas tarde.");
            return;
          }
          localStorage.clear();
          localStorage.setItem("user-token", token);
          setTimeout(() => {
            navigate("/home");
          }, 500);
        }
      } catch (error) {
        alert("Oops! Ocurrió algun error.");
      }
      navigate("/home");
    }
    setIsLoggedIn(true);
  };
  useEffect(() => {
    checkUserToken();
  }, [isLoggedIn]);

  const onSubmit: SubmitHandler<Inputs> = async () => {
    try {
      const response = await axiosUserApi.get("/user/");
      if (response.data) {
        if (
          user.username === response.data?.username &&
          user.password === response.data?.password
        ) {
          const data = response.data;
          const token = data.token;
          if (!token) {
            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: "Por favor intentelo mas tarde!",
            });
            return;
          }
          localStorage.clear();
          localStorage.setItem("user-token", token);
          setTimeout(() => {
            Swal.fire({
              icon: "success",
              title: "Acceso correcto",
              showConfirmButton: false,
              timer: 1800,
            });
          }, 1000);
          setTimeout(() => {
            navigate("/home");
          }, 2000);
        } else {
          if (
            user.username !== response.data?.username &&
            user.password !== response.data?.password
          ) {
            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: "El usuario y contraseña son incorrectos!",
            });
          } else {
            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: "El usuario ó contraseña es incorrecto!",
            });
          }
        }
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Ocurrió algun error!",
      });
    }
  };

  const InputUser = (e: React.ChangeEvent<HTMLInputElement>, name: string) => {
    const {
      target: { value },
    } = e;
    setUser({
      ...user,
      [name]: value,
    });
  };

  return (
    <React.Fragment>
      <div className={styles.container}>
        <div className={styles.card}>
          <form
            onSubmit={handleSubmit(onSubmit)}
            className={styles.containerForm}
          >
            <div className={styles.form}>
              <p className={styles.titleLogin}>Login</p>
              <label className={styles.subTitlesLogin}>Username</label>
              <input
                {...register("username")}
                onChange={(e) => InputUser(e, "username")}
                className={styles.inputText}
              />
              <br />
              <label className={styles.subTitlesLogin}>Password</label>
              <input
                {...register("password")}
                onChange={(e) => InputUser(e, "password")}
                className={styles.inputText}
                type="password"
              />
              <br />
              <br />
              <input type="submit" className={styles.inputSubmit} />
            </div>
          </form>
        </div>
      </div>
    </React.Fragment>
  );
};
