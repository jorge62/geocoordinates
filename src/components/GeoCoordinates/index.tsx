import { axiosProvinceApi } from "api/axios";
import { Map } from "components/Map";
import { ReqLoggerProvinceApi } from "interceptors/interceptors";
import * as React from "react";
import { useEffect } from "react";
import { useState } from "react";
import Swal from "sweetalert2";
import styles from "./styles.module.scss";

export const Coordinates: React.FC = () => {
  const [province, setProvince] = useState<string>("");
  const [data, setData] = useState<
    { centroide: { lat: number; lon: number }; id: string; nombre: string }[]
  >([]);
  const [showMap, setShowMap] = useState<boolean>(false);

  useEffect(() => {
    setShowMap(false);
  }, [province]);

  const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { value },
    } = e;
    setProvince(value);
  };

  const handleCick = async (
    eventClick?: React.MouseEvent<HTMLButtonElement>,
    eventKey?: React.KeyboardEvent
  ) => {
    if (eventKey?.key === "Enter" || eventClick?.type === "click") {
      try {
        const response = await axiosProvinceApi.get(
          `/provincias?nombre=${province}`
        );
        /* eslint-disable */
        ReqLoggerProvinceApi;
        if (response?.data) {
          setData(() => response.data.provincias);
          setShowMap(true);
        }
        if (response.data?.total === 0) {
          return Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "No se encontró la provincia!",
          });
        }
      } catch (error) {
        console.error(error);
      }
    }
  };

  return (
    <React.Fragment>
      <div className={styles.container}>
        <div className={styles.formData}>
          <p className={styles.title}>Provincia</p>
          <input
            type="text"
            onChange={(e) => handleInput(e)}
            className={styles.input}
            onKeyDown={(e) => handleCick(undefined, e)}
          />
          <button
            onClick={(e) => handleCick(e, undefined)}
            className={styles.button}
            style={{
              backgroundColor: province.length > 0 ? "#8B0000" : "#6A6A6A",
            }}
          >
            Consultar
          </button>
        </div>
        <div className={styles.containerMap}>
          {data.length > 0 &&
            showMap &&
            data.map((mapa, index) => {
              return (
                <div className={styles.map} key={index}>
                  <p>{mapa.nombre}</p>
                  <Map {...mapa} />
                  <div className={styles.coordinates}>
                    <p>
                      <b>Latitud</b>: {mapa.centroide.lat}
                    </p>
                    <p>
                      <b>Longitud</b>: {mapa.centroide.lon}
                    </p>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    </React.Fragment>
  );
};
