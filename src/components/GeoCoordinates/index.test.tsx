import "@testing-library/jest-dom/extend-expect";
import { render, waitFor, screen } from "@testing-library/react";
import { Coordinates } from "./index";
import axios from "axios";

describe("<Coordinates/>", () => {
  test("Get data from apiProvinces", async () => {
    const province = "buenos";
    const res = await axios
      .get(`https://apis.datos.gob.ar/georef/api/provincias?nombre=${province}`)
      .then(function (response) {
        return response.data;
      });
    expect(res?.provincias.length > 0).toBeTruthy();
  });

  test("Get province", async () => {
    const province = "Córdo";

    const res = await axios
      .get(`https://apis.datos.gob.ar/georef/api/provincias?nombre=${province}`)
      .then(function (response) {
        return response.data;
      });
    res?.provincias.map((e: any) => {
      expect(e.nombre.toLowerCase()).toContain(province.toLowerCase());
    });
  });
});
