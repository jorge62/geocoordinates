import * as React from "react";
import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";

interface Props {
  centroide: {
    lat: number;
    lon: number;
  };
  id: string;
  nombre: string;
}

export const Map: React.FC<Props> = (mapa) => {
  return (
    <React.Fragment>
      <MapContainer
        style={{ height: 400, width: 600 }}
        center={[mapa.centroide.lat, mapa.centroide.lon]}
        zoom={8}
        dragging={true}
        doubleClickZoom={false}
        scrollWheelZoom={true}
        attributionControl={true}
        zoomControl={true}
      >
        <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
        <Marker position={[mapa.centroide.lat, mapa.centroide.lon]}>
          <Popup>
            <div style={{ minHeight: "300px", width: "40px" }}>My Popup</div>
          </Popup>
        </Marker>
      </MapContainer>
    </React.Fragment>
  );
};
