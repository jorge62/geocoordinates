import * as React from "react";
import styles from "./styles.module.scss";

export const NotFound = () => {
  document.body.style.backgroundColor = "black";
  return (
    <div className={styles.container}>
      <div className={styles.terminal}>
        <p className={styles.prompt}>
          La página que solicitó no se puede encontrar ahora.
        </p>
        <p className={styles.newoutput}></p>
      </div>
    </div>
  );
};
