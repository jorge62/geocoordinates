import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

export const ProtectedRoute = (props: any) => {
  const navigate = useNavigate();
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const checkUserToken = () => {
    const userToken = localStorage.getItem("user-token");
    if (!userToken || userToken === "undefined") {
      setIsLoggedIn(false);
      return navigate("/auth/login");
    }
    setIsLoggedIn(true);
  };
  useEffect(() => {
    checkUserToken();
  }, []);
  return <React.Fragment>{isLoggedIn ? props.children : null}</React.Fragment>;
};
