import React, { useEffect, useState } from "react";
import { Footer } from "./components/UI/footer";
import { Header } from "./components/UI/header";
import { Outlet } from "react-router-dom";

function App() {
  ///check loggin
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const checkUserToken = () => {
    const userToken = localStorage.getItem("user-token");
    if (!userToken || userToken === "undefined") {
      setIsLoggedIn(false);
    }
    setIsLoggedIn(true);
  };
  useEffect(() => {
    checkUserToken();
  }, [isLoggedIn]);
  return (
    <React.Fragment>
      {isLoggedIn && <Header />}
      <Outlet />
      {isLoggedIn && <Footer />}
    </React.Fragment>
  );
}
export default App;
