import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { Routes, Route } from "react-router-dom";
import { Auth } from "components/Auth/index";
import { ProtectedRoute } from "util/ProtectedRoute";
import { Coordinates } from "components/GeoCoordinates";
import { NotFound } from "./pages/NotFound404";
import { Home } from "pages/Home";
import "./index.css";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

root.render(
  <BrowserRouter basename={"/coordinates"}>
    <Routes>
      <Route path="/auth/login" element={<Auth />} />
      <Route path="/home" element={<Home />}>
        <Route
          path=""
          element={
            <ProtectedRoute>
              <Coordinates />
            </ProtectedRoute>
          }
        />
      </Route>
      <Route path="*" element={<NotFound />} />
    </Routes>
  </BrowserRouter>
);
