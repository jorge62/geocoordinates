import * as AxiosLogger from "axios-logger";
import { axiosProvinceApi, axiosUserApi } from "api/axios";

export const ReqLoggerUserApi = axiosUserApi.interceptors.request.use(
  (request) => {
    console.log("Request:", JSON.stringify(request, null, 2));
    return AxiosLogger.requestLogger(request);
  }
);

export const ReqLoggerProvinceApi = axiosProvinceApi.interceptors.request.use(
  (request) => {
    console.log("Request:", JSON.stringify(request, null, 2));
    return AxiosLogger.requestLogger(request);
  }
);
